package a;

public class Main {
  public static void main(String[] args) {
    Bee firstBee = new Bee(0, 0, 1, 1);
    Bee secondBee = new Bee(0, 2, 1, 3);
    Bee thirdBee = new Bee(0, 4, 1, 5);

    Bee forthBee = new Bee(2, 0, 3, 1);
    Bee fifthBee = new Bee(2, 2, 3, 3);
    Bee sixBee = new Bee(2, 4, 3, 5);

    Bee seventhBee = new Bee(4, 0, 5, 1);
    Bee eighthBee = new Bee(4, 2, 5, 3);
    Bee ninthBee = new Bee(4, 4, 5, 5);

    new Thread(firstBee).start();
    new Thread(secondBee).start();
    new Thread(thirdBee).start();
    new Thread(forthBee).start();
    new Thread(fifthBee).start();
    new Thread(sixBee).start();
    new Thread(seventhBee).start();
    new Thread(eighthBee).start();
    new Thread(ninthBee).start();

  }
}
