package a;

import lombok.Getter;

import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Bee implements Runnable {

  private static char[][] forest = new char[][]{
      {' ', '*', ' ', ' ', ' ', ' '},
      {' ', '*', ' ', '*', ' ', ' '},
      {' ', '*', '*', ' ', ' ', ' '},
      {' ', ' ', ' ', ' ', '*', ' '},
      {' ', '*', ' ', '*', '*', '*'},
      {'*', ' ', ' ', ' ', 'X', '*'},
  };

  private static boolean found = false;

  private int x1, y1, x2, y2;

  public Bee(int x1, int y1, int x2, int y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  @Override
  public void run() {
    synchronized (forest) {
      System.out.println(Thread.currentThread().getId() + " start");
      Integer x = x1, y = y1;
      Queue<Pair<Integer, Integer>> queue = new ConcurrentLinkedQueue<>();

      queue.add(new Pair<>(x, y));

      while (!queue.isEmpty()) {
        if (found) {
          System.out.println("stop");
          return;
        }
        Pair<Integer, Integer> now = queue.poll();
        x = now.getFirst();
        y = now.getSecond();

        if (forest[x][y] == 'X') {
          found = true;
          System.out.println("found");
          return;
        }

        if (x + 1 <= x2 && forest[x + 1][y] != '*') {
          queue.add(new Pair<>(x + 1, y));
        }
        if (x - 1 >= x1 && forest[x - 1][y] != '*') {
          queue.add(new Pair<>(x - 1, y));
        }
        if (y + 1 <= y2 && forest[x][y + 1] != '*') {
          queue.add(new Pair<>(x, y + 1));
        }
        if (y - 1 >= y1 && forest[x][y - 1] != '*') {
          queue.add(new Pair<>(x, y - 1));
        }

        forest[x][y] = '*';

      }
      System.out.println("not found");
    }
  }

}

class Pair<T, T1> {

  @Getter
  private T first;
  @Getter
  private T1 second;

  public Pair(T first, T1 second) {
    this.first = first;
    this.second = second;
  }



}
