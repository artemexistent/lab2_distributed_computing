package b;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class Main {

  public static List<Integer> jobFor2 = Collections.synchronizedList(new ArrayList<>());
  public static List<Integer> jobFor3 = Collections.synchronizedList(new ArrayList<>());
  public static Random random = new Random();
  public static AtomicInteger cost = new AtomicInteger();

  public static void main(String[] args) {

    new Thread(new First()).start();
    new Thread(new Second()).start();
    new Thread(new Third()).start();

  }

}

class First implements Runnable {
  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      System.out.println("1 - start");
      Thread.sleep(5000);
      Main.jobFor2.add(Main.random.nextInt());
      System.out.println("1 - end");
    }
  }
}
class Second implements Runnable {
  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      if (!Main.jobFor2.isEmpty()) {
        System.out.println("2 - start");
        Integer cost = Main.jobFor2.get(0);
        Main.jobFor2.remove(0);
        Thread.sleep(30000);
        Main.jobFor3.add(cost);
        System.out.println("2 - end");
      }
    }
  }
}
class Third implements Runnable {
  @SneakyThrows
  @Override
  public void run() {
    while (true) {
      if (!Main.jobFor3.isEmpty()) {
        System.out.println("3 - start");
        Integer cost = Main.jobFor3.get(0);
        Main.jobFor3.remove(0);
        Thread.sleep(10000);
        System.out.println("3 - end");
      }
    }
  }
}
